# 这是提交脚本中的执行部分段落，用的bash语法
# 参考：https://blog.csdn.net/weixin_43145427/article/details/123818952

srun --cpu_bind=cores --distribution=block:cyclic octopus 1>gs.out 2>gs.err

echo ``display the first row:''
sed -n '1p' ./inp
sed -i '1s/gs/td/' inp
echo ``display the new first row:''
sed -n '1p' ./inp

srun --cpu_bind=cores --distribution=block:cyclic octopus 1>td.out 2>td.err
